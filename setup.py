from setuptools import setup

setup(name='cone_detector',
      version='0.3.1',
      description='Automatic cone detection software',
      url='https://gitlab.com/rmapbda/ConeDetector',
      author='Benjamin Davidson',
      author_email='ben.davidson6@googlemail.com',
      packages=['cone_detector', 'cone_detector.model', 'cone_detector.tensorflow_tools'],
      include_package_data=True,
      install_requires=[
          'numpy',
          'scipy',
          'scikit-image',
          'Pillow',
          'scikit-learn',
          'pandas',
          'imageio'
      ],
      entry_points={
          'console_scripts': ['cone_detector=cone_detector.entry:main'],
      },
      keywords='AOSLO photoreceptor localisation',
      )
