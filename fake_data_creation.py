import numpy as np
import imageio
import random
import os
import pandas as pd


from skimage.draw import circle
from uuid import uuid4
from cone_detector.constants import X_POS, Y_POS, IMAGE_PATH


data_folder = 'C:\\Users\\Ben\\PycharmProjects\\ConeDetector\\example_data'
image_folder = os.path.join(data_folder, 'images')
df_path = os.path.join(data_folder, 'data.csv')


def make_example():
    h = random.randint(333, 334)
    w = random.randint(333, 334)
    num_centers = random.randint(0, 50)

    image = np.zeros([h, w, 1], dtype=np.uint8)
    centers = np.random.random([num_centers, 2])*np.array([[w, h]])
    for center in centers:
        rr, cc = circle(center[1], center[0], radius=5, shape=(h, w))
        image[rr, cc] = 255

    return centers, image


def make_data():
    dataframe = []
    for _ in range(100):
        centers, image = make_example()
        im_path = os.path.join(image_folder, uuid4().hex + '.JPG')
        imageio.imsave(
            im_path,
            image)
        for x, y in list(centers):
            row = {
                IMAGE_PATH: im_path,
                X_POS: x,
                Y_POS: y}
            dataframe.append(row)
    df = pd.DataFrame(dataframe)
    df.to_csv(df_path, index=False)


if __name__ == '__main__':
    make_data()