
# Automatic Cone Localisation with MDLSTMs
This is an implementation of the method described in our [paper on cone localisation](https://www.nature.com/articles/s41598-018-26350-3). The backbone of our approach is multidimensional recurrent neural networks, which were first described in [Alex Graves paper](https://arxiv.org/abs/0705.2011). Our network is an LSTM variant of this approach with some bells and whistles for training stability and training speed.

If you use the code please cite our paper:
```
@Article{Davidson2018,
    author={Davidson, Benjamin
    and Kalitzeos, Angelos
    and Carroll, Joseph
    and Dubra, Alfredo
    and Ourselin, Sebastien
    and Michaelides, Michel
    and Bergeles, Christos},
    title={Automatic Cone Photoreceptor Localisation in Healthy and Stargardt Afflicted Retinas Using Deep Learning},
    journal={Scientific Reports},
    year={2018},
    volume={8},
    number={1},
    pages={7911},
    issn={2045-2322},
    doi={10.1038/s41598-018-26350-3},
    url={https://doi.org/10.1038/s41598-018-26350-3}
}
```
 
MDRNN             |  MDLSTM
:-------------------------:|:-------------------------:
![](./images/model.jpg)  |  ![](./images/alg.jpg)

## To install
To install you will need python, pip and tensorflow
1. [Download and install python](https://www.python.org/downloads/release/python-368/)
2. Open a terminal or command prompt `pip install git+https://gitlab.com/rmapbda/ConeDetector.git`
3. Install tensorflow.
    1. If you just want to use the model you can install tensorflow for cpu only `pip install tensorflow`
    2. If you want to train new models in a reasonable amount of time you will need to install tensorflow-gpu. [Instructions can be found here.](https://gitlab.com/rmapbda/ConeDetector.git)
## Using
After pip installing a command line utility will be created that lets you do things with the command `cone_detector --task TASK [OTHER ARGS]`. For a description of the available commands enter `cone_detector -h`. This provides the functionality to:
* Applying a trained model
* Preprocess your own data for use in training new models
* Train new models, following the training regime given in the paper. 
## Training a new model
* You must provide a csv file with the format `image_path, x, y`. This is a **single row per cone**, where `x=0` is left and `y=0` is the top.
* A toy example containing images and corresponding csv file is in `/example_data`.
* To monitor training you can run `tensorboard --logdir LOGDIR`. The location of `LOGDIR` will be displayed in the terminal when you initiate a training run.
* To train a new network using the toy data on my machine you do the following:

Process the data into a format for tensorflow
```
cone_detector --task process_data --train_record C:\Users\Ben\Desktop\train.record --val_record C:\Users\Ben\Desktop\val.record --csv_file .\example_data\data.csv
```
Train the new model
```
cone_detector --task train_new --train_record C:\Users\Ben\Desktop\train.record --val_record C:\Users\Ben\Desktop\val.record --model_name myTest
```
Apply the model (this outputs the segmentations, to output only centers change `--post_process true`)
```
cone_detector --task apply_model --model_name myTest --image_folder C:\Users\Ben\PycharmProjects\ConeDetector\example_data\images\ --out_folder C:\Users\Ben\PycharmProjects\ConeDetector\example_data\out --post_process false
```
### Locating cones
![](./images/example.jpg)
### Toy dataset
![](./images/toy.gif)