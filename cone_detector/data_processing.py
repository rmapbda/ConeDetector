from cone_detector.constants import *
from cone_detector.model.constants import *
from cone_detector.utilities import read_tif

from skimage.draw import circle
import numpy as np
import tensorflow as tf
import pandas as pd


class DataProcessor:

    def __init__(self, df, training_record_path, validation_record_path):
        self.df = df
        self.training_writer = tf.python_io.TFRecordWriter(training_record_path)
        self.validation_writer = tf.python_io.TFRecordWriter(validation_record_path)
        self.sess = tf.Session()
        self._validate_df()

    def _create_split(self):
        self.df = self.df.sample(frac=1.)
        is_training = np.random.random([self.df.shape[0]]) < 0.8
        self.df.loc[:, TRAIN_SPLIT] = is_training

    def _validate_df(self):
        for field in [IMAGE_PATH, X_POS, Y_POS]:
            message = 'must have a column {} in dataframe'.format(field)
            assert field in self.df.keys(), message

    def _group_image_centers(self):
        gpby = self.df.groupby(IMAGE_PATH)
        xs = gpby[X_POS].apply(list)
        ys = gpby[Y_POS].apply(list)
        df = pd.concat([xs, ys], axis=1)
        df[CONE_CENTERS] = df.apply(lambda x: list(zip(x[X_POS], x[Y_POS])), axis=1)
        self.df = df

    def process_dataframe(self):
        self._group_image_centers()
        self._create_split()
        for path, centers, is_train in zip(self.df.index, self.df[CONE_CENTERS], self.df[TRAIN_SPLIT]):
            if os.path.exists(path):
                self._process_single_datum(path, centers, is_train)
            else:
                print('Skipping {} file does not exist'.format(os.path.basename(path)))

    @staticmethod
    def _build_mask(height, width, centers):
        mask = np.zeros([height, width], np.bool)
        for x, y in centers:
            rr, cc = circle(y, x, radius=DEFAULT_RADIUS, shape=(height, width))
            mask[rr, cc] = True
        return mask

    def _process_single_datum(self, path, centers, is_train):
        image = read_tif(path)
        height, width = image.shape[0], image.shape[1]
        mask = DataProcessor._build_mask(height, width, centers)
        centers = np.array(centers)
        record = self._to_tf_record(image, mask, centers, height, width)
        self.write_record(record, is_train)

    @staticmethod
    def _build_feat_dict(image, mask, centers, height, width):
        # we convert everything to a np array to use
        # its tostring method for serialisation
        features = {
            IMAGE_FEAT: image,
            MASK_FEAT: mask,
            HEIGHT_FEAT: height,
            WIDTH_FEAT: width,
            CENTERS_FEAT: centers}
        return features

    @staticmethod
    def _bytes_feature(value):
        return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))

    @staticmethod
    def _int64_feature(value):
        return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))

    def write_record(self, record, is_train):
        serialised = record.SerializeToString()
        if is_train:
            self.training_writer.write(serialised)
        else:
            self.validation_writer.write(serialised)

    def _convert_to_tf_features(self, features):
        for key in features:
            if key in [HEIGHT_FEAT, WIDTH_FEAT]:
                features[key] = DataProcessor._int64_feature(features[key])
            else:
                features[key] = DataProcessor._bytes_feature(
                    self.sess.run(tf.serialize_tensor(features[key])))
        return features

    def _to_tf_record(self, image, mask, centers, height, width):
        features = DataProcessor._build_feat_dict(
            tf.constant(image),
            tf.constant(mask),
            tf.constant(centers),
            height,
            width)
        features = self._convert_to_tf_features(features)
        tf_record = tf.train.Example(features=tf.train.Features(feature=features))
        return tf_record


def read_record(record_path):
    import matplotlib.pyplot as plt

    tf.enable_eager_execution()

    feature_description = {
        HEIGHT_FEAT: tf.FixedLenFeature([], tf.int64),
        WIDTH_FEAT: tf.FixedLenFeature([], tf.int64),
        MASK_FEAT: tf.FixedLenFeature([], tf.string),
        CENTERS_FEAT: tf.FixedLenFeature([], tf.string),
        IMAGE_FEAT: tf.FixedLenFeature([], tf.string)}

    def parse(ex):
        return tf.parse_single_example(ex, feature_description)

    dataset = tf.data.TFRecordDataset(record_path)
    dataset = dataset.shuffle(2)
    dataset = dataset.map(parse)
    for feats in dataset:
        centers = tf.parse_tensor(feats[CENTERS_FEAT], tf.float64).numpy()
        image = tf.parse_tensor(feats[IMAGE_FEAT], tf.uint8).numpy()
        mask = tf.parse_tensor(feats[MASK_FEAT], tf.bool).numpy()
        image = image[:, :, 0]*mask
        plt.imshow(image)
        plt.scatter(centers[:, 0], centers[:, 1])
        plt.show()
