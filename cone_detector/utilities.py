import numpy as np
from PIL import Image


def read_tif(image_path):
    im = Image.open(image_path)
    if len(im.split()) > 1:
        im = im.split()[0]
    im = np.array(im.getdata(), dtype=np.uint8).reshape(im.size[1], im.size[0], 1)
    return im


def crop_center(img, size):
    y, x = img.shape
    startx = x // 2 - (size // 2)
    starty = y // 2 - (size // 2)
    return img[starty:starty + size, startx:startx + size]


def list_to_array(h, w, center_list):
    """centers are row, column"""
    array = np.zeros([h, w], dtype=np.uint8)
    for r in center_list:
        array[r[0, 0], r[0, 1]] = 1

    return array


def array_to_list(arr):
    non_zero = np.transpose(np.nonzero(arr > 0))
    non_zero_list = [(x[0, 0], x[0, 1]) for x in np.split(non_zero, non_zero.shape[0], axis=0)]
    return non_zero_list


def array_to_grayscale(array):
    array = array.astype(np.float32)
    array = array - array.min()
    array = array / array.max()
    array = array * 255.
    return array.astype(np.uint8)


def expand_centers(centers, height, width, radius=5):
    y, x = np.ogrid[-radius:radius + 1, -radius:radius + 1]
    disk = x ** 2 + y ** 2 <= radius ** 2
    disk = disk.astype(np.uint8)

    segmentation = np.zeros([height, width], dtype=np.uint8)

    for row, col in centers:
        row = int(row)
        col = int(col)
        for offset_row in range(2 * radius + 1):
            for offset_col in range(2 * radius + 1):
                row_pos = row - radius + offset_row
                col_pos = col - radius + offset_col
                try:
                    if segmentation[row_pos, col_pos] == 0:
                        segmentation[row_pos, col_pos] = disk[offset_row, offset_col]
                except IndexError:
                    continue

    return segmentation


def location_array(centers, height, width):
    arr = np.zeros([height, width], dtype=np.uint8)

    for row, col in centers:
        row = int(row)
        col = int(col)
        arr[row, col] = 1

    return arr


###############################################################
# Dice for centers
###############################################################


def get_neighbour_thresh(centers):
    intra_center_dist = np.linalg.norm(centers[:, None] - centers[None, :], axis=1)
    upper = np.triu(intra_center_dist, 1)
    distance = np.median(upper[upper != 0.])
    return min(20., 0.75*distance)


def get_neighbours(train, query):
    # [train, query]
    distance_arr = np.linalg.norm(train[:, None] - query[None, :], axis=2)
    # drop the first as this is itself
    closest_neighbours = np.argsort(distance_arr, axis=1)
    distances = np.take_along_axis(distance_arr, closest_neighbours, axis=1)
    return closest_neighbours, distances


def center_dice(true_centers,  estimated_centers):
    distance_thresh = get_neighbour_thresh(true_centers)
    if estimated_centers.shape[0] == 0 or true_centers.shape[0] == 0:
        return 0.
    ordered_neighbours, distances = get_neighbours(estimated_centers, true_centers)
    TOO_FAR = -1
    ordered_neighbours = np.where(distances < distance_thresh, ordered_neighbours, TOO_FAR)

    # doesnt account for double matches but much simpler and good enough
    nearest = ordered_neighbours[:, 0]
    fps = np.sum(nearest == TOO_FAR)
    tps = nearest.shape[0] - fps
    fns = np.logical_not(np.isin(np.arange(true_centers.shape[0]), nearest)).sum()

    dice = 2*tps/(2*tps + fps + fns)
    return dice


if __name__ == '__main__':

    i = read_tif('C:\\Users\\Ben\\PycharmProjects\\ConeDetector\\example_data\\images\\1014R_split_0020_ref_109_lps_8_lbss_8_sr_n_21_cropped_part1.tif')
    print(i.shape)




