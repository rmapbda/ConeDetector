import os

IMAGE_PATH = 'image_path'
CONE_CENTERS = 'centers'
X_POS = 'x'
Y_POS = 'y'
MASK_PATH = 'mask_path'
TRAIN_SPLIT = 'train_split'
DEFAULT_RADIUS = 5


# data locations
PACKAGE_DIREC = os.path.dirname(os.path.realpath(__file__))
MODEL_DIREC = os.path.join(PACKAGE_DIREC, 'models')
DATA_DIREC = os.path.join(PACKAGE_DIREC, 'datasets')
LOG_DIREC = os.path.join(PACKAGE_DIREC, 'training_logs')

# model names
NO_MODEL = 'None'
PAPER_MODEL = 'paperModel'
NO_DATA = 'No data'

# file extension
TIF = 'tif'

# model patch size
SIZE = 128
