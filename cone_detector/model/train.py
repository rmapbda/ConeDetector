from cone_detector.tensorflow_tools.training_model import TrainingModel
from cone_detector.tensorflow_tools.train import Trainer
from cone_detector.tensorflow_tools.constants import LOG_DIR, MODEL_FILE_DIR

from cone_detector.model.model import PaperConeNetwork
from cone_detector.model.data_loader import AOSLOLoader
from cone_detector.model.constants import *
from cone_detector.model.forward_model import build_forward_model, get_callable_model
from cone_detector.model.post_processing import PostProcessor
from cone_detector.utilities import center_dice

import pickle
import tensorflow as tf
import numpy as np
import os


def lr_schedule(init_learn, global_step):
    return init_learn


def optimiser(x):
    return tf.train.RMSPropOptimizer(x)


def view_input():
    g = tf.get_default_graph()
    images = g.get_tensor_by_name(IMAGE_TENSOR_NAME + ':0')
    image = tf.summary.image('image', images, max_outputs=1)
    return image


def view_mask():
    g = tf.get_default_graph()
    images = g.get_tensor_by_name(MASK_TENSOR_NAME + ':0')
    image = tf.summary.image('mask', images[:, :, :, 1:], max_outputs=1)
    return image


def view_probs():
    g = tf.get_default_graph()
    images = g.get_tensor_by_name(PROBABILITIES_TENSOR_NAME + ':0')
    image = tf.summary.image('probabilities', images[:, :, :, 1:], max_outputs=1)
    return image


def train_model(model_name, epochs, batch_size, train_record, val_record, num_gpus=1):
    data = AOSLOLoader(train_record, val_record, batch_size)
    net = PaperConeNetwork()
    model = TrainingModel(net, data)
    print('assuming {} gpus'.format(num_gpus))
    if num_gpus > 1:
        model.build_network_multiple(num_gpus, MULTI_GPU_OUT_NAME)
        model.add_loss(lambda: net.loss(MULTI_GPU_OUT_NAME))
    else:
        model.build_network()
        model.add_loss(net.loss)

    util_optimiser = model.build_optimisation(optimiser, INIT_LEARN, lr_schedule)
    model.add_optimiser(util_optimiser, multi_gpu=num_gpus > 1)

    model.add_standard_loss_summaries()
    model.add_intra_epoch_summary(view_input)
    model.add_intra_epoch_summary(view_mask)
    model.add_intra_epoch_summary(view_probs)
    train = Trainer(model_name, model, [VALIDATION_SET, TRAINING_SET])
    train.add_early_stopping(VALIDATION_SET, max_stall=30)
    print('**********************************************************************************')
    print('**********************************************************************************')
    print('You can monitor training by running the following command in a shell or cmd prompt')
    print('tensorboard --logdir {}'.format(LOG_DIR))
    print('**********************************************************************************')
    print('**********************************************************************************')
    train.train(epochs)


def get_search_grid():
    m, n = 20, 20
    thresh_range = np.linspace(0.2, 1., n)
    sigma_range = np.linspace(0.8, 3., m)
    sigma_grid = np.tile(sigma_range[:, None], (1, n))
    thresh_grid = np.tile(thresh_range[None, :], (m, 1))
    return np.stack([sigma_grid, thresh_grid], axis=2), (m, n), sigma_range, thresh_range


def get_dice_at_point(search_point, probability_pixel_is_cone, centers):
    import matplotlib.pyplot as plt
    estimated_centers = PostProcessor.get_centers_static(
        probability_pixel_is_cone,
        search_point[0],
        search_point[1])

    if estimated_centers is None:
        return 0.
    dice = center_dice(centers, estimated_centers)

    return dice


def get_best_hyperparams(all_image_dices, sigma_vals, thresh_vals, grid_shape):
    all_image_dices = np.stack(all_image_dices)
    mean_dice_per_point = all_image_dices.mean(axis=0)
    best_hyper_linear = np.argmax(mean_dice_per_point)
    print('best dice on train {}'.format(mean_dice_per_point[best_hyper_linear]))
    sigma_id, thresh_id = np.unravel_index(best_hyper_linear, grid_shape)
    sigma = sigma_vals[sigma_id]
    thresh = thresh_vals[thresh_id]
    return sigma, thresh


def get_best_sigma_and_thresh(segmenter, training_record):
    loader = AOSLOLoader(training_record, validation_record=None)
    training_data = loader.get_train_data_for_hyperparam()
    data_iterator = training_data.make_one_shot_iterator()

    search_grid, grid_shape, sigma_vals, thresh_vals = get_search_grid()
    linear_grid = np.reshape(search_grid, [-1, 2])
    all_image_dices = []
    k = 0
    print('Estimating best post processing parameters over')
    print('a maximum of 400 images.')
    with tf.Session() as sess:
        image, centers = data_iterator.get_next()
        while True and k < 400:
            try:
                im_np, center_np = sess.run([image, centers])
            except tf.errors.OutOfRangeError:
                break
            print('Estimating params for image {}'.format(k))
            k += 1
            im_np = im_np[:, :, 0]
            probability_pixel_is_cone = segmenter(im_np)
            dices_for_image = np.zeros([grid_shape[0] * grid_shape[1]], dtype=np.float32)

            for i, search_point in enumerate(linear_grid):
                dice = get_dice_at_point(search_point, probability_pixel_is_cone, center_np)
                dices_for_image[i] = dice
            all_image_dices.append(dices_for_image)

    sigma, thresh = get_best_hyperparams(all_image_dices, sigma_vals, thresh_vals, grid_shape)
    return sigma, thresh


def save_to_model(sigma, thresh, model_name):
    model_dir = os.path.join(MODEL_FILE_DIR, model_name)
    with open(os.path.join(model_dir, HYPER_PARAM_PICKLE), 'wb') as pfile:
        pickle.dump({
            SIGMA: sigma,
            THRESH: thresh},
            pfile)


def train_hyper_params(model_name, training_record):
    # builds from most recent checkpoint
    build_forward_model(model_name, run=None, variable_map=None)
    segmenter = get_callable_model(model_name, post_process=False)
    sigma, thresh = get_best_sigma_and_thresh(segmenter, training_record)
    save_to_model(sigma, thresh, model_name)


if __name__ == '__main__':
    example_folder = 'C:\\Users\\Ben\\PycharmProjects\\ConeDetector\\example_data'
    train_record = os.path.join(example_folder, 'train.record')
    validation_record = os.path.join(example_folder, 'validation.record')
    # train_model(
    #     'test',
    #     30,
    #     4,
    #     train_record,
    #     validation_record)
    train_hyper_params('test', train_record)