from cone_detector.model.forward_model import build_forward_model


def convert_from_old():
    # from tensorflow.python.tools import inspect_checkpoint as chkp
    # # print all tensors in checkpoint file
    # chkp.print_tensors_in_checkpoint_file(
    #     "C:\\Users\\Ben\\PycharmProjects\\ConeDetector\\cone_detector\\data\\models\\paperModel\\model-1",
    #     tensor_name='',
    #     all_tensors=True)
    variable_map = {
        'conv_0/conv_weights': 'conv_0/Conv/weights',
        'conv_0/conv_bias': 'conv_0/Conv/biases',
        'conv_1/conv_weights': 'conv_1/Conv/weights',
        'conv_1/conv_bias': 'conv_1/Conv/biases',
        'classifier_weights': 'fully_connected/Conv/weights',
        'classifier_bias': 'fully_connected/Conv/biases',
        'classifier_weights2': 'fully_connected/Conv_1/weights',
        'classifier_bias_2': 'fully_connected/Conv_1/biases',
        'MD_0/c_bias': 'MD_0/c_bias',
        'MD_0/f1_bias': 'MD_0/f1_bias',
        'MD_0/f2_bias': 'MD_0/f2_bias',
        'MD_0/i_bias': 'MD_0/i_bias',
        'MD_0/o_bias': 'MD_0/o_bias',
        'MD_0/hidden_weights': 'MD_0/hidden_weights',
        'MD_0/input_weights': 'MD_0/input_weights',
        'MD_1/c_bias': 'MD_1/c_bias',
        'MD_1/f1_bias': 'MD_1/f1_bias',
        'MD_1/f2_bias': 'MD_1/f2_bias',
        'MD_1/i_bias': 'MD_1/i_bias',
        'MD_1/o_bias': 'MD_1/o_bias',
        'MD_1/hidden_weights': 'MD_1/hidden_weights',
        'MD_1/input_weights': 'MD_1/input_weights',
    }
    build_forward_model('paperModel', 1, variable_map)
