from cone_detector import utilities
from cone_detector.model.constants import *
from cone_detector.constants import PAPER_MODEL
from cone_detector.tensorflow_tools.constants import MODEL_FILE_DIR

import scipy.ndimage.filters as filters
import numpy as np
import os
import pickle
from skimage.measure import regionprops, label



class PostProcessor:

    def __init__(self, model_name):
        self.model_name = model_name

    def get_hyperparam_path(self):
        return os.path.join(MODEL_FILE_DIR, self.model_name, HYPER_PARAM_PICKLE)

    def get_info(self):
        with open(self.get_hyperparam_path(), 'rb') as pfile:
            hyperparams = pickle.load(pfile)
            return hyperparams

    def get_centers(self, prob_map):
        if self.model_name == PAPER_MODEL:
            estimated_centroids = PostProcessor.get_centers_static(prob_map, JOINT_SIGMA, JOINT_THRESH)
            if len(estimated_centroids) > DENSITY * prob_map.shape[0] * prob_map.shape[1]:
                estimated_centroids = PostProcessor.get_centers_static(prob_map, HEALTHY_SIGMA, HEALTHY_THRESH)
            else:
                estimated_centroids = PostProcessor.get_centers_static(prob_map, STGD_SIGMA, STGD_THRESH)
        else:
            hyperparams = self.get_info()
            estimated_centroids = PostProcessor.get_centers_static(
                prob_map,
                hyperparams[SIGMA],
                hyperparams[THRESH])
        return estimated_centroids

    @staticmethod
    def smooth(prob_map, sigma):
        smoothed = filters.gaussian_filter(prob_map, sigma)
        return smoothed

    @staticmethod
    def im_extended_max(I):
        max_vals = filters.maximum_filter(I, size=LOCAL_MAX_KERNEL)
        mask = (max_vals - I) <= 0
        return mask

    @staticmethod
    def reject_weak(mask, value_array, thresh):
        centers = [x.centroid for x in regionprops(label(mask), intensity_image=value_array) if
                     x.max_intensity > thresh]
        return centers

    @staticmethod
    def join_close_centers(centers, shape):
        """
            make mask with disks rather than points
        """
        if not centers:
            return []

        dialiated = utilities.expand_centers(centers, shape[0], shape[1], radius=DILATE_RADIUS)
        centers = [x.centroid for x in regionprops(label(dialiated))]

        return centers

    @staticmethod
    def remove_from_border(centers, height, width):
        new_list = []
        border_thresh = BORDER_THRESH
        for row in centers:
            if border_thresh < row[0] < height - border_thresh and border_thresh < row[1] < width - border_thresh:
                new_list.append(row)
        return new_list

    @staticmethod
    def get_centers_static(I, sigma, thresh):
        smoothed = PostProcessor.smooth(I, sigma)
        mask = PostProcessor.im_extended_max(smoothed)
        centers = PostProcessor.reject_weak(mask, smoothed, thresh)
        joined = PostProcessor.join_close_centers(centers, I.shape)
        without_border = PostProcessor.remove_from_border(joined, I.shape[0], I.shape[1])
        try:
            # this gives us in terms of (x, y)
            estimated_centroids = np.array(without_border)[:, ::-1]
        except IndexError:
            estimated_centroids = None
        return estimated_centroids
