from cone_detector.tensorflow_tools.constants import MODEL_FILE_DIR
from cone_detector.tensorflow_tools.inference import ForwardModel, InferenceBuilder
from cone_detector.model.model import PaperConeNetwork
from cone_detector.model.post_processing import PostProcessor
from cone_detector.model.constants import *
from cone_detector.constants import *
from cone_detector.utilities import read_tif

import tensorflow as tf
import numpy as np


def process_single_image(inputs):
    image = tf.cast(tf.expand_dims(inputs[IMAGE_TENSOR_NAME], 2), tf.float32)
    image = tf.expand_dims(image, axis=0)
    image = image - tf.reduce_mean(image)
    return {IMAGE_TENSOR_NAME: image}


def build_forward_model(model_name, run, variable_map):
    net = PaperConeNetwork()
    builder = InferenceBuilder(model_name, run, net)
    builder.to_optmised_graph(
        outputs=PROBABILITIES_TENSOR_NAME,
        inputs={IMAGE_TENSOR_NAME: {'shape': [None, None], 'dtype': tf.uint8}},
        preprocess=process_single_image,
        variable_map=variable_map)


def get_callable_model(model_name, post_process=False, fliplr=False):
    if post_process:
        p = PostProcessor(model_name)
    model_path = os.path.join(MODEL_FILE_DIR, model_name, 'optimised_model.pb')
    f = ForwardModel(model_path, {NETWORK_FORWARD_OUT: PROBABILITIES_TENSOR_NAME})

    def image_to_centers(image):
        if fliplr:
            image = np.fliplr(image)
        probs = f({IMAGE_TENSOR_NAME: image})[NETWORK_FORWARD_OUT][0]
        if fliplr:
            probs = np.fliplr(probs[:, :, 1])
        if post_process:
            centers = p.get_centers(probs[:, :, 1])
            return centers
        else:
            return probs[:, :, 1]

    return image_to_centers


if __name__ == '__main__':
    pass