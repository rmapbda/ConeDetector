from cone_detector.tensorflow_tools.dataset import Dataset
from cone_detector.model.constants import *

import tensorflow as tf


class AOSLOLoader(Dataset):
    data_features = {
        HEIGHT_FEAT: tf.FixedLenFeature([], tf.int64),
        WIDTH_FEAT: tf.FixedLenFeature([], tf.int64),
        MASK_FEAT: tf.FixedLenFeature([], tf.string),
        CENTERS_FEAT: tf.FixedLenFeature([], tf.string),
        IMAGE_FEAT: tf.FixedLenFeature([], tf.string)}

    def __init__(self, train_record, validation_record, batch_size=4):
        Dataset.__init__(self, )
        self.batch_size = batch_size
        self.train_record = train_record
        self.validation_record = validation_record

    @staticmethod
    def mask_feat_process(mask_feat):
        mask = tf.parse_tensor(mask_feat, tf.bool)
        mask = tf.cast(mask, tf.int32)
        mask = tf.one_hot(mask, depth=2, dtype=tf.float32)
        return mask

    @staticmethod
    def center_feat_process(center_feat):
        center_feat = tf.parse_tensor(center_feat, tf.float64)
        return center_feat

    @staticmethod
    def image_feat_process(image_feat):
        image = tf.parse_tensor(image_feat, tf.uint8)
        image = tf.cond(
            tf.equal(tf.rank(image), 2),
            lambda: tf.expand_dims(image, 2),
            lambda: image)
        image = tf.cast(image, tf.float32)
        return image

    @staticmethod
    def crop_to_size(image, mask):
        stacked = tf.concat([image, mask], axis=2)
        cropped = tf.image.resize_image_with_crop_or_pad(stacked, TRAINING_HEIGHT, TRAINING_WIDTH)
        image = cropped[:, :, 0]
        mask = cropped[:, :, 1:]
        return image, mask

    @staticmethod
    def process_for_image_and_mask(record):
        all_features = tf.parse_single_example(record, AOSLOLoader.data_features)
        image = AOSLOLoader.image_feat_process(all_features[IMAGE_FEAT])
        mask = AOSLOLoader.mask_feat_process(all_features[MASK_FEAT])
        image, mask = AOSLOLoader.crop_to_size(image, mask)
        image = image - tf.reduce_mean(image)
        image = tf.expand_dims(image, axis=2)

        return image, mask

    @staticmethod
    def process_for_training_image_and_mask(record):
        all_features = tf.parse_single_example(record, AOSLOLoader.data_features)
        image = AOSLOLoader.image_feat_process(all_features[IMAGE_FEAT])
        mask = AOSLOLoader.mask_feat_process(all_features[MASK_FEAT])

        image = image - tf.reduce_mean(image)
        concat_depth = tf.concat([image, mask], axis=2)
        cropped = tf.image.random_crop(
            concat_depth,
            tf.constant([TRAINING_HEIGHT, TRAINING_WIDTH, 3]),)
        image, mask = cropped[:, :, :1], cropped[:, :, 1:]

        return image, mask

    @staticmethod
    def process_for_image_and_centers(record):
        all_features = tf.parse_single_example(record, AOSLOLoader.data_features)
        image = tf.parse_tensor(all_features[IMAGE_FEAT], tf.uint8)
        centers = AOSLOLoader.center_feat_process(all_features[CENTERS_FEAT])
        return image, centers

    def process_dataset(self, dataset, training=False):
        dataset = dataset.shuffle(100)
        if training:
            dataset = dataset.map(AOSLOLoader.process_for_training_image_and_mask, num_parallel_calls=4)
        else:
            dataset = dataset.map(AOSLOLoader.process_for_image_and_mask, num_parallel_calls=4)
        dataset = dataset.batch(self.batch_size)
        dataset = dataset.prefetch(1)
        return dataset

    def get_train_data_for_hyperparam(self,):
        dataset = tf.data.TFRecordDataset(self.train_record)
        dataset = dataset.map(AOSLOLoader.process_for_image_and_centers)
        return dataset

    def _make_train_data(self,):
        dataset = tf.data.TFRecordDataset(self.train_record)
        dataset = self.process_dataset(dataset, training=True)
        return dataset

    def _make_validation_data(self,):
        dataset = tf.data.TFRecordDataset(self.validation_record)
        dataset = self.process_dataset(dataset)
        return dataset

    def build_pipeline(self, *args):
        print('building input pipeline for graph...')
        train_data = self._make_train_data()
        val_data = self._make_validation_data()
        self.add_tf_dataset(TRAINING_SET, train_data)
        self.add_tf_dataset(VALIDATION_SET, val_data)
        image, mask = self.iterator.get_next()
        self.add_gettable_tensor(IMAGE_TENSOR_NAME, image)
        self.add_gettable_tensor(MASK_TENSOR_NAME, mask)

    def is_training_set(self, name):
        return name == TRAINING_SET

    def gpu_batch(self, inputs, gpus):
        num_images = tf.shape(inputs[IMAGE_TENSOR_NAME])[0]
        apps_per_batch = num_images // gpus
        split_inputs = []
        for k in range(gpus):
            a = k * apps_per_batch
            b = (k + 1) * apps_per_batch
            if k < gpus - 1:
                gpu_input = {
                    IMAGE_TENSOR_NAME: inputs[IMAGE_TENSOR_NAME][a:b],
                    MASK_TENSOR_NAME: inputs[MASK_TENSOR_NAME][a:b]}
            else:
                gpu_input = {
                    IMAGE_TENSOR_NAME: inputs[IMAGE_TENSOR_NAME][a:],
                    MASK_TENSOR_NAME: inputs[MASK_TENSOR_NAME][a:]}
            split_inputs.append(gpu_input)
        return split_inputs
