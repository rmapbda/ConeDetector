from cone_detector.model.two_d_LSTM import MD_parallel
from cone_detector.tensorflow_tools.network import Network
from cone_detector.model.constants import *

import tensorflow as tf
import tensorflow.contrib.slim as slim


def weighted_dice_loss(predictions, labels):
    """
        use a balanced loss with

        (1-ratio)*lossTerm(pixel_belongin_to_cell) + (ratio)*lossTerm(pixel_background)

        this will penalise incorrect cells more than incorrect background (if ratio < 0.5)
        and as such we get some class balancing. If we use the original ratio withou
        thresholding the model is too keen to predict cells at any cost as this ratio
        can be 0.

    """
    _, height, width, _ = labels.get_shape().as_list()
    label_channels = 2
    reshaped_labels = tf.reshape(labels, [-1, height * width, label_channels])
    reshaped_preds = tf.reshape(predictions, [-1, height * width, label_channels])

    multed = tf.reduce_sum(reshaped_labels * reshaped_preds, axis=1)
    summed = tf.reduce_sum(reshaped_labels + reshaped_preds, axis=1)
    r0 = tf.reduce_sum(reshaped_labels[:, :, 0], axis=1)
    r1 = tf.cast(height * width, tf.float32) - r0
    w0 = 1. / (r0 * r0 + 1.)
    w1 = 1. / (r1 * r1 + 1.)
    numerators = w0 * multed[:, 0] + w1 * multed[:, 1]
    denom = w0 * summed[:, 0] + w1 * summed[:, 1]
    dices = 1. - 2. * numerators / denom
    loss = tf.reduce_mean(dices)
    return loss


class PaperConeNetwork(Network):
    def build_network(self, inputs, **kwargs):
        net = inputs['images']
        units = 32
        with tf.variable_scope('conv_0'):
            net = slim.conv2d(net, 1, 3, activation_fn=tf.tanh)
        with tf.variable_scope('MD_0'):
            net = MD_parallel(net, units, 'leaky', 0.25, flatten_out=True)
            tf.identity(net[:, :, :, 50:51], 'check')
        with tf.variable_scope('conv_1'):
            net = slim.conv2d(net, 1, 3, activation_fn=tf.tanh)
        with tf.variable_scope('MD_1'):
            net = MD_parallel(net, units, 'leaky', 0.25, flatten_out=True)
        with tf.variable_scope('fully_connected'):
            net = slim.conv2d(net, 64, 1, activation_fn=tf.nn.relu)
            net = slim.conv2d(net, 2, 1, activation_fn=None)

        tf.identity(net, LOGITS_TENSOR_NAME)
        tf.identity(tf.nn.softmax(net), PROBABILITIES_TENSOR_NAME)

    def loss(self, name=PROBABILITIES_TENSOR_NAME):
        g = tf.get_default_graph()
        labels = g.get_tensor_by_name(MASK_TENSOR_NAME + ':0')
        preds = g.get_tensor_by_name(name + ':0')
        return weighted_dice_loss(preds, labels)