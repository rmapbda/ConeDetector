import argparse
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

from cone_detector.model.train import train_hyper_params, train_model
from cone_detector.model.forward_model import get_callable_model
from cone_detector.data_processing import DataProcessor
from cone_detector.constants import *
from cone_detector.utilities import read_tif

import imageio
import numpy as np
import pandas as pd


def train_new_model(model_name, batch_size, train_record, val_record, num_gpus):
    train_model(
        model_name,
        epochs=500,
        batch_size=batch_size,
        train_record=train_record,
        val_record=val_record,
        num_gpus=num_gpus)

    train_hyper_params(model_name, train_record)


def apply_model_to_image(model, path):
    image = read_tif(path)
    if image.ndim == 3:
        image = image[:, :, 0]
    centers_or_probs = model(image)
    return centers_or_probs


def apply_model_to_folder(folder, out_path, model_name=PAPER_MODEL, fliplr=False, post_process=True,):
    image_paths = [os.path.join(folder, x) for x in os.listdir(folder) if x.split('.')[-1].lower() in ['tiff', 'tif', 'jpg']]
    apply_model_to_images(
        image_paths,
        out_path,
        model_name=model_name,
        fliplr=fliplr,
        post_process=post_process,)


def apply_model_to_images(image_paths, out_path, model_name, fliplr, post_process,):
    if model_name != PAPER_MODEL:
        fliplr = False
    else:
        print('You are using the paper model')
        print('this was trained with the bright lobes')
        print('on the left. If your setup has them on')
        print('right make sure to pass the command')
        print()
        print('--fliplr True')

    model = get_callable_model(model_name, post_process, fliplr)
    dataframe = []
    for path in image_paths:
        # save segmentation
        if not post_process:
            segmentation = apply_model_to_image(model, path)
            segmentation *= 255
            segmentation = segmentation.astype(np.uint8)
            name = os.path.basename(path)
            saved_path = os.path.join(out_path, name)
            imageio.imsave(saved_path, segmentation)
        else:
            centers = apply_model_to_image(model, path)
            if centers is None:
                centers = []
            row = {
                IMAGE_PATH: path,
                CONE_CENTERS: list(centers)
            }
            dataframe.append(row)
    df = pd.DataFrame(dataframe)
    df.to_pickle(out_path + 'center.pickle')
    df.to_csv(out_path + 'center.csv')


def pre_process_data(csv_file, train_path, val_path, replace=None):
    try:
        df = pd.read_csv(
            csv_file,
            names=[IMAGE_PATH, X_POS, Y_POS],
            dtype={X_POS: np.float64, Y_POS: np.float64})
        if replace is not None:
            df[IMAGE_PATH] = df[IMAGE_PATH].map(lambda x: x.replace(replace[0], replace[1]))
    except ValueError as e:
        print(e)
        print('will try to read csv without header')
        df = pd.read_csv(
            csv_file,
            names=[IMAGE_PATH, X_POS, Y_POS],
            dtype={X_POS: np.float64, Y_POS: np.float64},
            header=0)
        if replace is not None:
            df[IMAGE_PATH] = df[IMAGE_PATH].map(lambda x: x.replace(replace[0], replace[1]))
    processor = DataProcessor(df, train_path, val_path)
    processor.process_dataframe()

TRAIN_NEW_MODEL = 'train_new'
APPLY_MODEL = 'apply_model'
PROCESS_DATA = 'process_data'

TASKS = [
    TRAIN_NEW_MODEL,
    APPLY_MODEL,
    PROCESS_DATA
]


def main():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        '--task',
        type=str)
    parser.add_argument(
        '--csv_file',
        type=str)
    parser.add_argument(
        '--out_folder',
        type=str)
    parser.add_argument(
        '--model_name',
        type=str,
        default=PAPER_MODEL)
    parser.add_argument(
        '--train_record',
        type=str,)
    parser.add_argument(
        '--val_record',
        type=str,)
    parser.add_argument(
        '--bright_on_left',
        type=str,
        default='false')
    parser.add_argument(
        '--post_process',
        type=str,
        default='true')
    parser.add_argument(
        '--num_gpus',
        type=int,
        default=1)
    parser.add_argument(
        '--batch_size',
        type=int,
        default=4)
    parser.add_argument(
        '--image_folder',
        type=str)
    args = parser.parse_args()

    if args.task not in TASKS:
        raise ValueError('Operation must be in {} but is {}'.format(TASKS, args.task))

    if args.task == TRAIN_NEW_MODEL:
        train_new_model(
            args.model_name,
            args.batch_size,
            args.train_record,
            args.val_record,
            args.num_gpus)
    elif args.task == PROCESS_DATA:
        pre_process_data(
            args.csv_file,
            args.train_record,
            args.val_record)
    elif args.task == APPLY_MODEL:
        assert args.bright_on_left.lower() in ['false', 'true'], 'bright must be true or false'
        assert args.post_process.lower() in ['false', 'true'], 'bright must be true or false'
        post_process = args.post_process.lower() == 'true'
        bright_on_left = args.bright_on_left.lower() == 'true'
        apply_model_to_folder(
            args.image_folder,
            args.out_folder,
            args.model_name,
            bright_on_left,
            post_process)

